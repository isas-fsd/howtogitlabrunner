#!/usr/bin/env bash

#
# This script create a release containg the README.pdf file from the
# https://gitlab.epfl.ch/idevelop/howtogitlabrunner/ project.
#

set -e -x

PROJECT_ID=1298

# Print out the environment
# env

echo "Hello from deploy.sh"
echo "My token is $MY_TOKEN"

# Should display "gitlab-runner"
whoami

# Create a release name
RELEASE_V=v$(date '+%s')
echo "Creating release: ${RELEASE_V}"

# Tagging the version
git tag -f -a -m "My sweet release $(date '+%Y-%m-%d %H:%M:%S')" ${RELEASE_V}

# Be sure to have a remote to push to
git remote set-url origin "git@gitlab.epfl.ch:idevelop/howtogitlabrunner.git"
# And actually push the tags
git push -f --tags

# @domq's trick to be able to see the curl response in stderr
tee_stderr="perl -pe STDERR->print(\$_);warn(\$!)if(\$!)"

# Uploading the README.pdf file
UPLOADED_FILE_URL="$(curl --request POST --header "Private-Token: ${MY_TOKEN}" --form "file=@README.pdf" "https://gitlab.epfl.ch/api/v4/projects/$PROJECT_ID/uploads" | $tee_stderr | jq -r .url)"

# Create the release until it finally work...
for retry in $(seq 1 5); do
    if curl --request POST \
        --header 'Content-Type: application/json' \
        --header "Private-Token: ${MY_TOKEN}" \
        --data '{"name": "New README release", "tag_name": "'${RELEASE_V}'", "description": "Release with the PDF [README.pdf]('${UPLOADED_FILE_URL}')"}' \
        "https://gitlab.epfl.ch/api/v4/projects/${PROJECT_ID}/releases" \
        | $tee_stderr | jq -e ".created_at"
    then
        break
    else
        sleep 5
    fi
done
