# HowToGitLabRunner

A simple use case to demonstrate how to use a
[shell](https://docs.gitlab.com/runner/executors/shell.html)
[GitLab-Runner](https://docs.gitlab.com/runner) to transform this README.md into
README.pdf with [pandoc](https://pandoc.org/) and create a
GitLab's [release](https://gitlab.epfl.ch/idevelop/howtogitlabrunner/-/tags).


## Preamble

Doing this demo taught us some important lesson: we first tried to build the
PDF and push it into this repo. While it seems feasible, it not a good idea.
First of all, you want to keep your commits atomic, meaning that you don't want
a commit creates a new commit automatically. Secondly, the CI/CD mechanisms is
here to build and/or deploy a specific commit to other systems/environment, not
modifying the current one. We ended to do it the following way:

  1. One modify the README.md file;
  1. It trigger the build to create the README.pdf file;
  1. Then, a release is created, associating the README.pdf file to it.

That way, the things are kept as close as possible to a standard dev/stage/prod
environment but still in the same repo, keeping it simple to understand.

> :bulb: Before diving into it, you may want to check the
[documentation](https://docs.gitlab.com/ce/ci/README.html) and some [real world
examples](https://docs.gitlab.com/ce/ci/examples/README.html).


## Setup the "shell" runner

First of all, you need a runner on a machine. The installation process is
described here: https://docs.gitlab.com/runner/install/

It creates the gitlab-runner user on your system.

> :bulb: On some Linux, you may want to
remove the `~/.bash_logout` file to avoid the issue
[4092](https://gitlab.com/gitlab-org/gitlab-runner/issues/4092) — thanks me
later.

The last step explains how to register the runner with `sudo gitlab-runner
register`. Answer the questions (the runner key is found on your Project >
Settings > CI/CD > Runner), choosing
[shell](https://docs.gitlab.com/runner/executors/shell.html) as
[executor](https://docs.gitlab.com/runner/executors/). It will create the
`/etc/gitlab-runner/config.toml` file which looks like that:

```
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "X1"
  url = "https://gitlab.epfl.ch"
  token = "tQqmy2TFSZoJQfm-83_W"
  executor = "shell"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
```

On the runner configuration page of the project, the runner tags are defined to
be used by default. In the `.gitlab-ci.yaml` files, all the jobs are set to be
run with the tags "test" — please double check that this tag is defined in the
runner configuration, or allow your runner to be ran without any tags.

> :bulb: Read more about tags here: https://docs.gitlab.com/ee/ci/runners/#using-tags

## Check and edit the `.gitlab-ci.yaml`

The whole pipeline/stage/job configuration stands in the `.gitlab-ci.yaml` file.
A lot of documentation is provided by GitLab: https://docs.gitlab.com/ce/ci/.

In short, this file defines the pipeline process, which contain stages. A stage
can contains jobs, which describes what to do. Jobs can be parallel and manual,
as of the "deploy_to_production" job.

> :bulb: Note that if you want to keep "produced" files between jobs, you have to use the
[artifacts](https://docs.gitlab.com/ce/user/project/pipelines/job_artifacts.html) system.

The file of this project define 4 stages:
  - test: just prints out some env variables.
  - build: creates the README.pdf.
  - deploy: creates the release via the `deploy.sh` script. Need a variable.
  - control: final steps, does nothing but echoing a string.


![project's pipeline](./img/project_pipeline.jpg "project's pipeline")


## Set the variable

To be able to use the API to create a release, you need to have a [Personal
Access
Tokens](https://docs.gitlab.com/ce/user/profile/personal_access_tokens.html)
which can be created in your [personal
account](https://gitlab.epfl.ch/profile/personal_access_tokens). In the
`deploy.sh` script, it's used as the `MY_TOKEN` variable. Once you get the
token, there are 2 ways to use it:

  - define it into the project (Project > Settings > CI/CD > Variables), which
    means that other persons of the project will use it.
  - define it when manually launching the pipeline: only you will be able to
    use it but automatic trigger will fail.

This is OK for this demo or small projects, but a better way to do it is to
create a "bot" user to whom you give some right on your repository and create
the personal access token from it.

## Observe

Now it time that you try out the pipeline. Either change something to this file
(I'm sure there's a lot of errors and missing/incorrect information) and push to
the repo, or go into your project CI/CD pipelines and use the "Run pipeline"
button.

Once the deploy stage is finished, you should be able to see the release:

![project's release](./img/release.png "project's release")

Also, you can manually continue the pipeline by clicking "Deploy to production"
— wait few seconds and the whole pipeline should turn green.

![pipeline's pass](./img/passed.png "pipeline's pass")
